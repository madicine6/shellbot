#!/bin/bash

token=''
admin_id=''
tele_url="https://api.telegram.org/bot$token"
timeout=10
output_lines=50
last_id=0

send_message() {
	curl -s "$tele_url/sendMessage" \
		--data-urlencode "chat_id=$admin_id" \
		--data-urlencode "reply_to_message_id=$message_id" \
		--data-urlencode "text=$1"
}

while true; do
	updates=$(curl -s "$tele_url/getUpdates" \
		--data-urlencode "offset=$(( $last_id + 1 ))" \
		--data-urlencode "timeout=60")
	updates_count=$(echo "$updates" | jq -r ".result | length")
	last_id=$(echo "$updates" | jq -r ".result[$(( "$updates_count" - 1 ))].update_id")
	for ((i=0; i<"$updates_count"; i++)); {
		{
		user_id="$(echo "$updates" | jq ".result[$i].message.from.id")"
		chat_id="$(echo "$updates" | jq ".result[$i].message.chat.id")"
		message_id="$(echo "$updates" | jq ".result[$i].message.message_id")"
		message_text="$(echo "$updates" | jq ".result[$i].message.text" | sed --sandbox 's#\\"#"#g;s#\\\\#\\#g;s/^"//;s/"$//')"

		[[ $user_id == $admin_id ]] &&
			[[ $admin_id == $chat_id ]] && {
				stdout="$(timeout "$timeout" bash -c "$message_text")"

				if [[ $? == 0 ]]; then
					stdout="$(echo "$stdout" | tail -n "$output_lines")"
					send_message "$stdout"
				else
					send_message "Operation timed out."
				fi
			}
		} &
	}
done